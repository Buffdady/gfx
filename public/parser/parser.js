function parser(filename, name) {

    let fs = require('fs');
    let vertices = [];
    let textureCoordinate = [];
    let VertexIndices = [];
    let normalIndices = [];
    let textureIndices = [];
    let vertexNormal = [];
    let vertexNormalsOrdered = [];
    let vertexTextureOrdered = [];


    let contents = fs.readFileSync("public/obj/" + filename, 'utf8');

    let splittedByN = contents.split("\n");
    let single = [];
    let splittedBySlash = "";

    for (let i = 0; i < splittedByN.length; i++) {
        single = splittedByN[i].split(' ');
        if (single[0] === 'v') {
            vertices.push(single[0 + 1]);
            vertices.push(single[0 + 2]);
            vertices.push(single[0 + 3]);
        }
        if (single[0] === 'vt') {
            textureCoordinate.push(single[0 + 1]);
            textureCoordinate.push(single[0 + 2]);
        }
        if (single[0] === 'vn') {
            vertexNormal.push(single[0 + 1]);
            vertexNormal.push(single[0 + 2]);
            vertexNormal.push(single[0 + 3]);
        }
        if (single[0] === 'f') {
            //vertices
            //1
            splittedBySlash = single[1].split('/');
            VertexIndices.push(splittedBySlash[0] - 1);


            //2
            splittedBySlash = single[2].split('/');
            VertexIndices.push(splittedBySlash[0] - 1);

            //3
            splittedBySlash = single[3].split('/');
            VertexIndices.push(splittedBySlash[0] - 1);
            //Texture
            //1
            splittedBySlash = single[1].split('/');
            textureIndices.push(splittedBySlash[1] - 1);


            //2
            splittedBySlash = single[2].split('/');
            textureIndices.push(splittedBySlash[1] - 1);

            //3
            splittedBySlash = single[3].split('/');
            textureIndices.push(splittedBySlash[1] - 1);

            //Normals
            //1
            splittedBySlash = single[1].split('/');
            normalIndices.push(splittedBySlash[2] - 1);


            //2
            splittedBySlash = single[2].split('/');
            normalIndices.push(splittedBySlash[2] - 1);

            //3
            splittedBySlash = single[3].split('/');
            normalIndices.push(splittedBySlash[2] - 1);


        }

    }

    //ordering
    for (let i = 0; i < normalIndices.length; i++) {
        vertexNormalsOrdered[VertexIndices[i] * 3] = vertexNormal[normalIndices[i] * 3];
        vertexNormalsOrdered[VertexIndices[i] * 3 + 1] = vertexNormal[normalIndices[i] * 3 + 1];
        vertexNormalsOrdered[VertexIndices[i] * 3 + 2] = vertexNormal[normalIndices[i] * 3 + 2];
    }
    for (let i = 0; i < VertexIndices.length; i++) {
        vertexTextureOrdered[VertexIndices[i] * 2] = textureCoordinate[textureIndices[i] * 2];
        vertexTextureOrdered[VertexIndices[i] * 2 + 1] = textureCoordinate[textureIndices[i] * 2 + 1];
    }

    fileObject = {

        vertices,
        textureCoordinate,
        vertexNormal,
        VertexIndices,
        textureIndices,
        normalIndices,
        vertexNormalsOrdered,
        vertexTextureOrdered
    };

    return fileObject;
}


module.exports.parser = parser;
