//attributes
attribute vec3 aNormal;
attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;
//uniform
uniform mat4 uPMatrix, uMVMatrix;
uniform mat3 uNormalMat;
uniform float uKd;   // Diffuse koeffizient
uniform float uKs;   // Specular  koeffizient
uniform float uShininessVal; // Shininess
uniform int uMode;
// Material color
uniform vec3 uDiffuseColor;
uniform vec3 uSpecularColor;
uniform vec3 uLightPos; // Light position
//varying
varying vec4 vColor; //color
varying vec3 vNormalInterp;
varying vec3 vVertPos;
varying vec2 vTextureCoord;


//
void main(){

  vec4 vertPos4 =uMVMatrix * vec4(aVertexPosition, 1.0);
  vTextureCoord = aTextureCoord;
  vVertPos = vec3(vertPos4) / vertPos4.w;
  vNormalInterp =uNormalMat *aNormal ;
  gl_Position = uPMatrix * vertPos4;
  //
  vec3 N = normalize(vNormalInterp);
  vec3 L = normalize(uLightPos - vec3(uMVMatrix));
  // Lambert's cosine law
  float lambertian = max(dot(N, L), 0.0);
  float specular = 0.0;
  if(lambertian >= 0.0) {
    vec3 R = reflect(-L, N);      // Reflected Light vector
    vec3 V = normalize(-vVertPos); // Vector to viewer
    // Compute the specular term
    float specAngle = max(dot(R, V), 0.0);
    specular = pow(specAngle, uShininessVal);
  //modes
  //0 =all
  //1=diffuse
  //2=specular
  if(uMode==0)
  {
  vColor = vec4(
               uKd * lambertian * uDiffuseColor +
               uKs * specular * uSpecularColor, 1.0);
}else if(uMode==1)
{
  vColor = vec4(uKd * lambertian * uDiffuseColor,1.0);
}else if(uMode==2)
{
  vColor = vec4(uKs * specular * uSpecularColor, 1.0);

}else{

 vColor = vec4(1.0,1.0,1.0, 1.0);
}
}else{
vColor = vec4(1.0,1.0,1.0, 1.0);
}}