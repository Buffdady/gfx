precision mediump float;
//uniforms
uniform sampler2D uSampler;
uniform float uKd;   // Diffuse koeffizient
uniform float uKs;   // Specular  koeffizient
uniform float uShininessVal; // Shininess
uniform int uMode;
// Material color
uniform vec3 uDiffuseColor;
uniform vec3 uSpecularColor;
uniform vec3 uLightPos; // Light position
//varying
varying vec2 vTextureCoord;
varying vec3 vNormalInterp;  //
varying vec3 vVertexPosition;       // Vertex position
varying vec3 worldPosition;
void main() {

  vec3 N = normalize(vNormalInterp);
  vec3 L = normalize(-uLightPos - worldPosition);
  // Lambert's cosine law
  float lambertian = max(dot(N, L), 0.0);
  float specular = 0.0;
  if(lambertian > 0.0) {
    vec3 R = reflect(-L, N);      // Reflected Light vector
    vec3 V = normalize(-vVertexPosition); // Vector to viewer
    // Compute the specular term
    float specAngle = max(dot(R, V), 0.0);
    specular = pow(specAngle, uShininessVal);
    if(uMode==0)
    {
    vec4 light=  vec4(uKd * lambertian * uDiffuseColor + uKs * specular * uSpecularColor, 1.0);
      gl_FragColor =light* texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));
      }else if(uMode==1)
      {
      vec4 light=  vec4(uKd * lambertian * uDiffuseColor , 1.0);
            gl_FragColor =light* texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));
      }else if(uMode==2)
      {
      vec4 light=vec4(uKs * specular * uSpecularColor,1.0);
                  gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t))*light;

      }else {
      gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));
      }
  }else{gl_FragColor =texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));}

}
