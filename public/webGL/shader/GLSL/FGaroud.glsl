precision mediump float;
 uniform sampler2D uSampler;
 varying vec2 vTextureCoord;
 varying vec4 vColor;

 void main() {
      gl_FragColor = vColor*texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));

 }
