class UpperJoint {
    constructor(id, joint, coordinates) {
        this.id = id;
        this.VertexPositionBuffer = gl.createBuffer();
        this.VertexIndexBuffer = gl.createBuffer();
        this.VertexTextureCoordBuffer = gl.createBuffer();
        this.VertexNormalBuffer = gl.createBuffer();
        this.coordinates = coordinates;
        this.parsedOBJ = joint;
        this.setBuffer(this.parsedOBJ.joint);
        this.objectM = mat4.create();
        this.init();
        this.position();
        this.setExtremities(this.coordinates)

    }

    setExtremities(crd) {
        this.upperArm = new UpperArm('UpperArmL', this.parsedOBJ, [crd[0] * 0.05, crd[1] * .2, 0]);

    }


    setBuffer(joint) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        let verticesArray = joint.vertices;
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verticesArray), gl.STATIC_DRAW);
        this.VertexPositionBuffer.itemSize = 3;
        this.VertexPositionBuffer.numItems = joint.vertices.length / this.VertexPositionBuffer.itemSize;


        //Indices
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.VertexIndexBuffer);
        let indexArray = joint.VertexIndices;
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexArray), gl.STATIC_DRAW);
        this.VertexIndexBuffer.itemSize = 1;
        this.VertexIndexBuffer.numItems = joint.VertexIndices.length;

        //texture
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexTextureCoordBuffer);
        let textureArray = joint.vertexTextureOrdered;
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureArray), gl.STATIC_DRAW);
        this.VertexTextureCoordBuffer.itemSize = 2;
        this.VertexTextureCoordBuffer.numItems = joint.vertexTextureOrdered.length / this.VertexTextureCoordBuffer.itemSize;

        //normals
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
        let normals = joint.vertexNormalsOrdered;
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);
        this.VertexNormalBuffer.itemSize = 3;
        this.VertexNormalBuffer.numItems = joint.vertexNormalsOrdered.length / this.VertexTextureCoordBuffer.itemSize;


    }


    setCoordinateSystem(boolean) {
        this.CoordinateSystemActive = boolean;
    }

    init() {
        mat4.identity(this.objectM);

    }
    ;


    rotate(x, y, z, denominatorOfPI) {

        mat4.rotate(this.objectM, this.objectM, Math.PI / denominatorOfPI, [x, y, z]);
    }


    position() {

        mat4.translate(this.objectM, this.objectM, this.coordinates);

    }

    move(x, y, z) {

        mat4.translate(this.objectM, this.objectM, [x, y, z]);
    }

    scale(x, y, z) {
        mat4.scale(this.objectM, this.objectM, [x, y, z]);
    }

    debuggingLogger(parrent) {

        console.log("Cube ObjecttM  " + this.id + "\n" + this.objectM);
        console.log("Parrent of" + this.id + "\n" + parrent);
        console.log("Mmatrix of" + this.id + "\n" + World);
    }


    setDebugger(boolean) {
        this.debugger = false;
    }

//to the graphiccard
    render(parrent) {
        //copy matrix usw.

        let backup = [];
        this.rotate(1, 0, 0, 100);
        mat4.copy(backup, parrent);
        mat4.multiply(mVMatrix, parrent, this.objectM);
        mat3.normalFromMat4(Normal, mVMatrix);

        //copy matrix end

        //resetBuffer
        //Vertex
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.VertexIndexBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
        //Texture
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexTextureCoordBuffer);

        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureCube);
        gl.uniform1i(shaderProgram.samplerUniform, 0);

        //Normals
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);

        gl.vertexAttribPointer(shaderProgram.normals, this.VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

        setMatrixUniforms();
        //reset Buffer end;
        gl.drawElements(gl.TRIANGLES, this.VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
        //if (this.CoordinateSystemActive) {
        //   if (false){
        //     this.coordinateSystem.setMMatrix(World);
        //     this.coordinateSystem.render();
        //     this.setCoordinateSystem(false)
        // }

        this.upperArm.render(parrent);


        mat4.copy(parrent, backup);


    }
}