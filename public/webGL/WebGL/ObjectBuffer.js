//MagicNumbers for Parser
// 0 =
//

let cubeVertexPositionBuffer;
let cubeVertexIndexBuffer;
let cubeVertexTextureCoordBuffer;

let coordinateVertexPositionBuffer;
let coordinateVertexTextureBuffer;

let VertexPositionBuffer;
let VertexIndexBuffer;
let VertexTextureCoordBuffer;
let VertexNormalsBuffer;
let TextureIndexBuffer;
let VertexNormalsIndexBuffer;

function initBuffers(parsedObjects) {


    initCoordinate();
    console.log(parsedObjects);
    torso(parsedObjects.torso)
}
function torso(parsedObjects) {
    //coordinates
    VertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, VertexPositionBuffer);
    let verticesArray = parsedObjects.vertices;
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verticesArray), gl.STATIC_DRAW);
    VertexPositionBuffer.itemSize = 3;
    VertexPositionBuffer.numItems = parsedObjects.vertices.length/VertexPositionBuffer.itemSize;


    //Indices
    VertexIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, VertexIndexBuffer);
    let indexArray = parsedObjects.VertexIndices;
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexArray), gl.STATIC_DRAW);
    VertexIndexBuffer.itemSize = 1;
    VertexIndexBuffer.numItems = parsedObjects.VertexIndices.length;

    //texture
    VertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, VertexTextureCoordBuffer);
    let textureArray = parsedObjects.vertexTextureOrdered;
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureArray), gl.STATIC_DRAW);
    VertexTextureCoordBuffer.itemSize = 2;
    VertexTextureCoordBuffer.numItems = parsedObjects.vertexTextureOrdered.length/VertexTextureCoordBuffer.itemSize;

    //normals
    VertexNormalsBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,VertexNormalsBuffer);
    let normals = parsedObjects.vertexNormalsOrdered;
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(normals),gl.STATIC_DRAW);
    VertexNormalsBuffer.itemSize = 3;
    VertexNormalsBuffer.numItems = parsedObjects.vertexNormalsOrdered.length/VertexTextureCoordBuffer.itemSize;

    //textureIndices
    TextureIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, TextureIndexBuffer);
    let textureIndexArray = parsedObjects.textureIndices;
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(textureIndexArray), gl.STATIC_DRAW);
    TextureIndexBuffer.itemSize = 1;
    TextureIndexBuffer.numItems = parsedObjects.textureIndices.length;

    //normalIndices
    VertexNormalsIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, VertexNormalsIndexBuffer);
    let NormalIndexArray = parsedObjects.normalIndices;
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(NormalIndexArray), gl.STATIC_DRAW);
    VertexNormalsIndexBuffer.itemSize = 1;
    VertexNormalsIndexBuffer.numItems = parsedObjects.normalIndices.length;
}

function initCoordinate() {

    coordinateVertexPositionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, coordinateVertexPositionBuffer);
    let vertices =
        [
            2.0, 0.0, 0.0,
            -2.0, 0.0, 0.0,

            0.0, 2.0, 0.0,
            0.0, -2.0, 0.0,

            0.0, 0.0, 2.0,
            0.0, 0.0, -2.0,
        ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    coordinateVertexPositionBuffer.itemSize = 3;
    coordinateVertexPositionBuffer.numItems = 6;
    //color
    coordinateVertexTextureBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, coordinateVertexTextureBuffer);
    let textureCoords = [
        0/255, 127/255,
        254/255, 127/255,
        127/255, 0/255,
        127/255, 254/255,

        1/255, 254/255,
        254/255, 1/255,

    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
    coordinateVertexTextureBuffer.itemSize = 2;
    coordinateVertexTextureBuffer.numItems = 6;






}

function initCube() {

    cubeVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
    let vertices = [
        // Front face
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0,

        // Back face
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,
        1.0, 1.0, -1.0,
        1.0, -1.0, -1.0,

        // Top face
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,

        // Bottom face
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0,

        // Right face
        1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,

        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    cubeVertexPositionBuffer.itemSize = 3;
    cubeVertexPositionBuffer.numItems = 24;



    cubeVertexIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
    let cubeVertexIndices = [
        0, 1, 2, 0, 2, 3,    // Front face
        4, 5, 6, 4, 6, 7,    // Back face
        8, 9, 10, 8, 10, 11,  // Top face
        12, 13, 14, 12, 14, 15, // Bottom face
        16, 17, 18, 16, 18, 19, // Right face
        20, 21, 22, 20, 22, 23  // Left face
    ];
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
    cubeVertexIndexBuffer.itemSize = 1;
    cubeVertexIndexBuffer.numItems = 36;

    cubeVertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
    let textureCoords = [
        // Front face
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,

        // Back face
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,

        // Top face
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,

        // Bottom face
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,

        // Right face
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,

        // Left face
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
    cubeVertexTextureCoordBuffer.itemSize = 2;
    cubeVertexTextureCoordBuffer.numItems = 24;
    // normals


}

