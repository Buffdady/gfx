let keyStroke = [];
let activeKey = 0;
let cameraControll = false;
let diffusionOn = true;
let specularOn = true;
let lightcontroll=false;


function handleKeyDown(event) {


    keyStroke[event.key] = true;

    switch (event.key) {

        case '1':
            activeKey = 0;
            break;
        case '2':
            activeKey = 1;
            break;
        case '3':
            activeKey = 2;
            break;
        case '4':
            activeKey = 3;
            break;
        case '5':
            activeKey = 4;
            break;
        case '6':
            activeKey = 5;
            break;
        case '7':
            activeKey = 6;
            break;
        case '8':
            activeKey = 7;
            break;
        case '9':
            activeKey = 8;
            break;
        case '0':
            activeKey = 10;
            break;
        case 'c':
            cameraControll = !cameraControll;
            break;
        case 'l':
            lightcontroll= !lightcontroll;
            break;

    }

}

function handleKeyUp(event) {

}


function handleKeys() {
    //keystrokes

    //shaders
    if (keyStroke['u']) {
        diffusionOn = !diffusionOn;
        if (ShaderChooser !== 0) {
            diffusionOn = true;
            specularOn = true;
            ShaderChooser = 0;
            initShaders();

        }
        setLightMode();
        console.log(uMode);
        keyStroke['u'] = false;



    }

    if (keyStroke['i']){
        specularOn = !specularOn;

        if (ShaderChooser !== 0) {

                diffusionOn = true;
                specularOn = true;
                ShaderChooser = 0;
                initShaders();

            }
            setLightMode();
            console.log(uMode);
            keyStroke['i'] = false;


        }
    if (keyStroke['o']) {
        diffusionOn = !diffusionOn;
        if (ShaderChooser !== 1) {
            diffusionOn = true;
            specularOn = true;
            ShaderChooser = 1;
            initShaders();


        }
        setLightMode();
        console.log(uMode);
        keyStroke['o'] = false;


    }
    if (keyStroke['p']) {
        specularOn = !specularOn;
        if (ShaderChooser !== 1) {
            diffusionOn = true;
            specularOn = true;
            ShaderChooser = 1;
            initShaders();


        }
        setLightMode();
        console.log(uMode);
        keyStroke['p'] = false;


    }


    if (cameraControll&&lightcontroll)
    {
        alert("Camera and Light cant' be activated at the same time, both will be reset to off!\n" +
            "if problems accrue please reload \" F5\"");
        cameraControll=false;
        lightcontroll=false;
    }
//Light
    if(lightcontroll&&!cameraControll)
    {

        if (keyStroke['w'] === true) {
            lightTransfomrations.rotate(1, 0, 0, 4);
            keyStroke['w'] = false;
        }
        if (keyStroke['s'] === true) {

            lightTransfomrations.rotate(1, 0, 0, -4);
            keyStroke['s'] = false;
        }
        if (keyStroke['e'] === true) {

            lightTransfomrations.rotate(0, 1, 0, 4);
            keyStroke['e'] = false;

        }
        if (keyStroke['q'] === true) {

            lightTransfomrations.rotate(0, 1, 0, -4);

            keyStroke['q'] = false;
        }
        if (keyStroke['a'] === true) {

            lightTransfomrations.rotate(0, 0, 1, 4);

            keyStroke['a'] = false;
        }

        if (keyStroke['d'] === true) {

            lightTransfomrations.rotate(0, 0, 1, -4);

            keyStroke['d'] = false;
        }
        if (keyStroke['ArrowUp'] === true) {
            console.log("in l");
            lightTransfomrations.move(0, 1, 0);
            keyStroke['ArrowUp'] = false;
        }
        if (keyStroke['ArrowLeft'] === true) {

            lightTransfomrations.move(-1, 0, 0);
            keyStroke['ArrowLeft'] = false;
        }
        if (keyStroke['ArrowDown'] === true) {

            lightTransfomrations.move(0, -1, 0);
            keyStroke['ArrowDown'] = false;
        }
        if (keyStroke['ArrowRight'] === true) {

            lightTransfomrations.move(1, 0, 0);

            keyStroke['ArrowRight'] = false;
        }
        if (keyStroke[','] === true) {

            lightTransfomrations.move(0, 0, 1);
            keyStroke[','] = false;
        }

        if (keyStroke['.'] === true) {

            lightTransfomrations.move(0, 0, -1);
            keyStroke['.'] = false;
        }
    }else  if (cameraControll&&!lightcontroll) {

        if (keyStroke['w'] === true) {

            cameraTransformation.rotate(1, 0, 0, -4);
            keyStroke['w'] = false;
        }
        if (keyStroke['s'] === true) {

            cameraTransformation.rotate(1, 0, 0, 4);
            keyStroke['s'] = false;
        }
        if (keyStroke['e'] === true) {

            cameraTransformation.rotate(0, 1, 0, 4);
            keyStroke['e'] = false;

        }
        if (keyStroke['q'] === true) {

            cameraTransformation.rotate(0, 1, 0, -4);

            keyStroke['q'] = false;
        }
        if (keyStroke['a'] === true) {

            cameraTransformation.rotate(0, 0, 1, -4);

            keyStroke['a'] = false;
        }

        if (keyStroke['d'] === true) {

            cameraTransformation.rotate(0, 0, 1, 4);

            keyStroke['d'] = false;
        }
        if (keyStroke['ArrowUp'] === true) {
            console.log("in cam");

            cameraTransformation.move(0, -1, 0);
            keyStroke['ArrowUp'] = false;
        }
        if (keyStroke['ArrowLeft'] === true) {

            cameraTransformation.move(1, 0, 0);
            keyStroke['ArrowLeft'] = false;
        }
        if (keyStroke['ArrowDown'] === true) {

            cameraTransformation.move(0, 1, 0);
            keyStroke['ArrowDown'] = false;
        }
        if (keyStroke['ArrowRight'] === true) {

            cameraTransformation.move(-1, 0, 0);

            keyStroke['ArrowRight'] = false;
        }
        if (keyStroke[','] === true) {

            cameraTransformation.move(0, 0, -1);
            keyStroke[','] = false;
        }

        if (keyStroke['.'] === true) {

            cameraTransformation.move(0, 0, 1);
            keyStroke['.'] = false;
        }

    }else if (!cameraControll&&!lightcontroll) {

        if (activeKey === 10) {

            if (keyStroke['x'] === true) {


                worldTransformations.scale(0.9, 1.0, 1.0);
                keyStroke['x'] = false;
            }

            if (keyStroke['X'] === true) {

                worldTransformations.scale(1.1, 1.0, 1.0);
                keyStroke['X'] = false;
            }

            if (keyStroke['y'] === true) {

                console.log(activeKey);
                worldTransformations.scale(1.1, 0.9, 1.0);
                keyStroke['y'] = false;
            }

            if (keyStroke['Y'] === true) {

                console.log(activeKey);
                worldTransformations.scale(1.0, 1.1, 1.0);
                keyStroke['Y'] = false;
            }
            if (keyStroke['z'] === true) {

                console.log(activeKey);
                worldTransformations.scale(1.0, 1.0, 0.9);
                keyStroke['z'] = false;
            }
            if (keyStroke['Z'] === true) {
                worldTransformations.scale(1.0, 1.0, 1.1);
                console.log(activeKey);
                keyStroke['Z'] = false;
            }
            if (keyStroke['w'] === true) {

                console.log(activeKey);
                worldTransformations.rotate(1, 0, 0, 4);
                keyStroke['w'] = false;
            }
            if (keyStroke['s'] === true) {

                worldTransformations.rotate(1, 0, 0, -4);
                keyStroke['s'] = false;
            }
            if (keyStroke['e'] === true) {

                worldTransformations.rotate(0, 1, 0, 4);
                keyStroke['e'] = false;

            }
            if (keyStroke['q'] === true) {

                worldTransformations.rotate(0, 1, 0, -4);

                keyStroke['q'] = false;
            }
            if (keyStroke['a'] === true) {

                worldTransformations.rotate(0, 0, 1, 4);

                keyStroke['a'] = false;
            }

            if (keyStroke['d'] === true) {

                worldTransformations.rotate(0, 0, 1, -4);

                keyStroke['d'] = false;
            }
            if (keyStroke['ArrowUp'] === true) {
                console.log("in world");
                worldTransformations.move(0, 1, 0);
                keyStroke['ArrowUp'] = false;
            }
            if (keyStroke['ArrowLeft'] === true) {

                worldTransformations.move(-1, 0, 0);
                keyStroke['ArrowLeft'] = false;
            }
            if (keyStroke['ArrowDown'] === true) {

                worldTransformations.move(0, -1, 0);
                keyStroke['ArrowDown'] = false;
            }
            if (keyStroke['ArrowRight'] === true) {

                worldTransformations.move(1, 0, 0);

                keyStroke['ArrowRight'] = false;
            }
            if (keyStroke[','] === true) {

                worldTransformations.move(0, 0, 1);
                keyStroke[','] = false;
            }

            if (keyStroke['.'] === true) {

                worldTransformations.move(0, 0, -1);
                keyStroke['.'] = false;
            }


            //single object
        } else {

            if (keyStroke['x'] === true) {


                myObjects.objects[activeKey].scale(0.9, 1.0, 1.0);
                keyStroke['x'] = false;
            }

            if (keyStroke['X'] === true) {

                myObjects.objects[activeKey].scale(1.1, 1.0, 1.0);
                keyStroke['X'] = false;
            }

            if (keyStroke['y'] === true) {

                console.log(activeKey);
                myObjects.objects[activeKey].scale(1.1, 0.9, 1.0);
                keyStroke['y'] = false;
            }

            if (keyStroke['Y'] === true) {

                console.log(activeKey);
                myObjects.objects[activeKey].scale(1.0, 1.1, 1.0);
                keyStroke['Y'] = false;
            }
            if (keyStroke['z'] === true) {

                console.log(activeKey);
                myObjects.objects[activeKey].scale(1.0, 1.0, 0.9);
                keyStroke['z'] = false;
            }
            if (keyStroke['Z'] === true) {
                myObjects.objects[activeKey].scale(1.0, 1.0, 1.1);
                console.log(activeKey);
                keyStroke['Z'] = false;
            }
            if (keyStroke['w'] === true) {

                myObjects.objects[activeKey].rotate(1, 0, 0, 4);
                keyStroke['w'] = false;
            }
            if (keyStroke['s'] === true) {

                myObjects.objects[activeKey].rotate(1, 0, 0, -4);
                keyStroke['s'] = false;
            }
            if (keyStroke['e'] === true) {

                myObjects.objects[activeKey].rotate(0, 1, 0, 4);
                keyStroke['e'] = false;

            }
            if (keyStroke['q'] === true) {

                myObjects.objects[activeKey].rotate(0, 1, 0, -4);

                keyStroke['q'] = false;
            }
            if (keyStroke['a'] === true) {

                myObjects.objects[activeKey].rotate(0, 0, 1, 4);

                keyStroke['a'] = false;
            }

            if (keyStroke['d'] === true) {

                myObjects.objects[activeKey].rotate(0, 0, 1, -4);

                keyStroke['d'] = false;
            }
            if (keyStroke['ArrowUp'] === true) {
                console.log("in in object");

                myObjects.objects[activeKey].move(0, 1, 0);
                keyStroke['ArrowUp'] = false;
            }
            if (keyStroke['ArrowLeft'] === true) {

                myObjects.objects[activeKey].move(-1, 0, 0);
                keyStroke['ArrowLeft'] = false;
            }
            if (keyStroke['ArrowDown'] === true) {

                myObjects.objects[activeKey].move(0, -1, 0);
                keyStroke['ArrowDown'] = false;
            }
            if (keyStroke['ArrowRight'] === true) {

                myObjects.objects[activeKey].move(1, 0, 0);

                keyStroke['ArrowRight'] = false;
            }
            if (keyStroke[','] === true) {

                myObjects.objects[activeKey].move(0, 0, 1);
                keyStroke[','] = false;
            }

            if (keyStroke['.'] === true) {

                myObjects.objects[activeKey].move(0, 0, -1);
                keyStroke['.'] = false;
            }
        }

    }
}

//modes
//0 =all
//1=diffuse
//2=specular
//set Light mode
function setLightMode() {

    if (diffusionOn && specularOn) {
        uMode = 0;
        setMatrixUniforms();
    } else if (diffusionOn) {
        uMode = 1;
        setMatrixUniforms();


    } else if (specularOn) {
        uMode = 2;
        setMatrixUniforms();


    } else {
        uMode = 99;
        setMatrixUniforms();

    }





}