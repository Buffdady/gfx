class Camera {
    constructor(id, xyz) {
        this.id = id;
        this.xyz = xyz;
        this.camera=mat4.create();
        this.invert=mat4.create();
        this.setIdenity();
        this.initCamera(xyz)

    }

    setIdenity() {

        mat4.identity(this.camera);

    }

    rotate(x, y, z, denominatorOfPI) {

        mat4.rotate(this.camera, this.camera, Math.PI / (denominatorOfPI*10), [x, y, z]);

    }

    move(x, y, z) {

        mat4.translate(this.camera, this.camera, [x, y, z]);
    }
    initCamera(xyz) {

        mat4.translate(this.camera, this.camera, xyz);
    }



//to the graphiccard
    render() {
        mat4.perspective(pMatrix, 90, gl.viewportWidth / gl.viewportHeight, 1);
        mat4.invert(cameraMatrix,this.camera);

        setMatrixUniforms();






    }


}

