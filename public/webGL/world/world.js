class World {
    constructor(id, xyz) {
        this.id = id;
        this.xyz = xyz;
        this.WorldTransformations = mat4.create();
        this.setIdenity();
    }

    setCoordinateSystem(boolean) {
        this.CoordinateSystemActive = boolean
    }

    setIdenity() {

        mat4.identity(this.WorldTransformations);
        this.position();
    }

    rotate(x, y, z, denominatorOfPI) {

        mat4.rotate(this.WorldTransformations, this.WorldTransformations, Math.PI / denominatorOfPI, [x, y, z]);

    }


    position() {

        mat4.translate(this.WorldTransformations, this.WorldTransformations, this.xyz);
        mat4.multiply(globalM, globalM, this.WorldTransformations);

    }

    move(x, y, z) {

        mat4.translate(this.WorldTransformations, this.WorldTransformations, [x, y, z]);
    }

    scale(x, y, z) {
        mat4.scale(this.WorldTransformations, this.WorldTransformations, [x, y, z]);
    }

//to the graphiccard
    render() {

        mat4.multiply(globalM,globalM,this.WorldTransformations);






    }


}