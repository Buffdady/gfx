class CoordinateSystem {
    constructor(id, coordinateVertexPositionBuffer, coordinateVertexTextureBuffer) {
        this.id = id;
        this.VertexPositionBuffer = coordinateVertexPositionBuffer;
        this.VertexColorBuffer = coordinateVertexTextureBuffer;
        this.xyz = 0;
        this.identity = [];
    }


    setMMatrix(identity) {


        mat4.copy(this.identity, identity);

    }


    //to the graphiccard
    render() {

        mat4.copy(World, this.identity);


        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);

        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexColorBuffer);
        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.VertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);


        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureCoord);
        gl.uniform1i(shaderProgram.samplerUniform, 0);
        gl.drawArrays(gl.LINES, 0, 6);

    }

}