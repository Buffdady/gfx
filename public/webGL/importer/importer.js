let importObjects = function (path) {
    return new Promise(function (resolve, reject) {
        let myRequest = new XMLHttpRequest;
        myRequest.open("GET", path);
        myRequest.onload = function () {
            if (this.status >= 200 && this.status < 300) {

                resolve(JSON.parse(myRequest.response));
                console.log("done");

            } else {
                reject("rejected");

            }


        };
        myRequest.send();

    });

};